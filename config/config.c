/*
    config - configuration file parser

    Copyright (C) 2019  Aidan Prangnell (AidoP)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A full copy of the license should be included with the source. If not see https://www.gnu.org/licenses/.
*/
#include "config.h"

config_option config[] = {
    { "background_colour", colour, { .colour = 0x0000FF } },
    { "foreground_colour", colour, { .colour = 0x00FF00 } },
    { "font_colour", colour, { .colour = 0x000000 } },
    { "error_colour", colour, { .colour = 0xFF0000 } },
    { "attention_colour", colour, { .colour = 0xFFFF00 } },
    { "font", string, { .string = "tewi" } }
};

void config_get_value(const char key[32], config_value* value) {
    for (int config_iteration = 0; config_iteration < sizeof(config); config_iteration++) {
        if (!strcmp(config[config_iteration].key, key)) {
            *value = config[config_iteration].value;
            return;
        }
    }
}

void config_get_value_by_index(int index, config_value* value) {
    *value = config[index].value;
}

void config_set_value(const char key[32], config_value value) {
    for (int config_iteration = 0; config_iteration < sizeof(config); config_iteration++) {
        if (!strcmp(config[config_iteration].key, key)) {
            config[config_iteration].value = value;
            return;
        }
    }
}