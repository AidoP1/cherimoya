#pragma once
/*
    config - configuration file parser

    Copyright (C) 2019  Aidan Prangnell (AidoP)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A full copy of the license should be included with the source. If not see https://www.gnu.org/licenses/.
*/

#include <xcb/xcb.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef enum {
    string      = 0b000001,
    character   = 0b000010,
    number      = 0b000100,
    colour      = 0b001000,
    boolean     = 0b010000,
    empty       = 0b100000  // Booleans cannot be empty
} config_type;

typedef union {
    char        string[4096];
    char        character;
    double      number;
    uint32_t    colour;
    bool        boolean;
} config_value;

typedef struct {
    char key[32];               // Keys are 32 characters in length
    config_type type;           // The expected value type
    config_value value;         // The options value
} config_option;

// The config
#define OPTIONS_COUNT 6
config_option config[OPTIONS_COUNT];

// Reads the config option into [value]
void config_get_value(const char key[32], config_value* value);

// Faster lookup for use when index is known
void config_get_value_by_index(int index, config_value* value);

// Sets the config value to [value]
void config_set_value(const char key[32], config_value value);


// -- Config Index constants
#define CONFIG_BACKGROUND_COLOUR 0
#define CONFIG_FOREGROUND_COLOUR 1
#define CONFIG_FONT_COLOUR 2
#define CONFIG_ERROR_COLOUR 3
#define CONFIG_ATTENTION_COLOUR 4
#define CONFIG_FONT 5