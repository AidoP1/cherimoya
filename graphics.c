/*
    graphics - functions for drawing with xcb

    Copyright (C) 2019  Aidan Prangnell (AidoP)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A full copy of the license should be included with the source. If not see https://www.gnu.org/licenses/.
*/
#include "graphics.h"
#include "draw.h"

// Define pages
cherimoya_page pages[10] = { };

void graphics_init() {
    // Open a connection on the default screen and display
    graphics_info.connection = xcb_connect(NULL, NULL);

    if (!graphics_info.connection) {
        fprintf (stderr, "Failed to connect to X\n");
        exit(EXIT_FAILURE);
    }

    // Get display info
    xcb_screen_iterator_t xcb_screen = xcb_setup_roots_iterator(xcb_get_setup(graphics_info.connection));

    graphics_info.screen = xcb_screen.data;

    // Initialise FreeType and FontConfig
    FT_Init_FreeType(&graphics_info.font.library);
    FcInit();
    
    // Create the font face
    graphics_init_font();

    // Perfectly safe as we can be sure that this title is less than 256 bytes
    strcpy(blank_page.title, "Unknown Page\0");
    blank_page.exists = true;
    blank_page.page_type = cherimoya_page_blank;
}

void graphics_init_font() {
    // Get the font name from the config
    config_value font_name;
    config_get_value_by_index(CONFIG_FONT, &font_name);

    // Use FontConfig to get the font name
    FcPattern* font_pattern = FcNameParse((FcChar8*)font_name.string);
    FcDefaultSubstitute(font_pattern);
    if (FcConfigSubstitute(NULL, font_pattern, FcMatchPattern) == FcFalse)
        fprintf(stderr, "Failed to substitute in font defaults.\n");

    FcResult match_result;
    FcPattern* matched_font = FcFontMatch(NULL, font_pattern, &match_result);
    FcPatternDestroy(font_pattern);

    if (match_result == FcResultMatch) {
        // Get the font file to load in
        FcValue font_file, font_face_index;
        if (FcPatternGet(matched_font, FC_FILE, 0, &font_file) != FcResultMatch)
            fprintf(stderr, "Unable to load font file for font matching string '%s'\n", font_name.string);
        if (FcPatternGet(matched_font, FC_INDEX, 0, &font_face_index) != FcResultMatch) {
            font_face_index.type = FcTypeInteger;
            font_face_index.u.i = 0;
        }
        
        // Use FreeType to rasterize the font
        FT_New_Face(graphics_info.font.library, (const char*)font_file.u.s, font_face_index.u.i, &graphics_info.font.face);
    } else if (match_result == FcResultNoMatch)
        fprintf(stderr, "Could not find a font matching '%s'\n", font_name.string);
    else
        fprintf(stderr, "Unknown error while matching font string '%s'\n", font_name.string);


    // Get info needed for the actual font drawing

    // Get suitable render format
    xcb_render_query_pict_formats_reply_t* glyph_all_formats = xcb_render_util_query_formats(graphics_info.connection);
    graphics_info.font.format = xcb_render_util_find_standard_format(glyph_all_formats, XCB_PICT_STANDARD_RGB_24);
}

void graphics_create_window() {
    // Prepare mask for the window, fills it with black and asks for exposure events
    uint32_t mask_values[2] = { graphics_info.screen->black_pixel, XCB_EVENT_MASK_EXPOSURE };

    // Create the window
    graphics_info.window.id = xcb_generate_id(graphics_info.connection);
    check_for_error( xcb_create_window(
        graphics_info.connection,
        XCB_COPY_FROM_PARENT,
        graphics_info.window.id,
        graphics_info.screen->root,
        0, 0, 480, 640,                 // x, y, width, height
        0,
        XCB_WINDOW_CLASS_INPUT_OUTPUT,
        graphics_info.screen->root_visual,
        XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK, mask_values
    ), "Unable to create window\nCode: %d\n", 1);

    // Create the graphics context
    graphics_info.window.context_id = xcb_generate_id(graphics_info.connection);
    mask_values[1] = 0;
    xcb_create_gc(graphics_info.connection, graphics_info.window.context_id, graphics_info.screen->root, XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES, mask_values);

    // Create the panel and blank pages
    blank_page.drawable_id = xcb_generate_id(graphics_info.connection);
    graphics_info.panel.id = xcb_generate_id(graphics_info.connection);

    graphics_info.panel.height = 64; // TODO: Actual calculation of panel height

    // Update the new window
    xcb_map_window(graphics_info.connection, graphics_info.window.id);
    xcb_flush(graphics_info.connection);
}

void graphics_start_loop() {
    xcb_generic_event_t* window_event;
    while ((window_event = xcb_wait_for_event (graphics_info.connection))) {

        switch (window_event->response_type & ~0x80) {
            case XCB_EXPOSE: {
                xcb_expose_event_t* expose_event = (xcb_expose_event_t *)window_event;
                // Update window size
                if (graphics_info.window.width != expose_event->width || graphics_info.window.width != expose_event->height) {
                    // Update the width and height
                    graphics_info.window.width = expose_event->width;
                    graphics_info.window.height = expose_event->height;

                    // Resize the pixmaps

                    // Calculate panel height - TODO
                    // Based on font etc

                    // Recreate the blank page at the new size
                    xcb_free_pixmap(graphics_info.connection, blank_page.drawable_id);
                    xcb_create_pixmap(graphics_info.connection, graphics_info.screen->root_depth, blank_page.drawable_id, graphics_info.window.id, graphics_info.window.width, graphics_info.window.height - graphics_info.panel.height);
                    
                    // Update blank page size
                    blank_page.width = graphics_info.window.width;
                    blank_page.height = graphics_info.window.height - graphics_info.panel.height;

                    // Recreate the panel at the new size
                    xcb_free_pixmap(graphics_info.connection, graphics_info.panel.id);
                    xcb_create_pixmap(graphics_info.connection, graphics_info.screen->root_depth, graphics_info.panel.id, graphics_info.window.id, graphics_info.window.width, graphics_info.panel.height);
                }

                // Update selected (visible) page
                cherimoya_page* page_to_draw = &blank_page;
                if (pages[selected_page].exists) {
                    page_to_draw = &pages[selected_page];
                    // Set the page size
                    page_to_draw->width = graphics_info.window.width;
                    page_to_draw->height = graphics_info.window.height - graphics_info.panel.height;
                    // Draw the page by its type
                    switch (pages[selected_page].page_type) {
                        case cherimoya_page_file_explorer: {
                            draw_file_explorer(pages[selected_page]);
                            break;
                        }
                        case cherimoya_page_home: {
                            break;
                        }
                        case cherimoya_page_status: {
                            break;
                        }
                        case cherimoya_page_blank: {
                            break;
                        }
                    }
                }

                // Update the top panel
                draw_panel();

                // Paste window elements onto the window
                xcb_copy_area(
                    graphics_info.connection,
                    page_to_draw->drawable_id,
                    graphics_info.window.id,
                    graphics_info.window.context_id,
                    0, 0, 0, graphics_info.panel.height,
                    page_to_draw->width, page_to_draw->height
                );
                xcb_copy_area(
                    graphics_info.connection,
                    graphics_info.panel.id,
                    graphics_info.window.id,
                    graphics_info.window.context_id,
                    0, 0, 0, 0,
                    graphics_info.window.width, graphics_info.panel.height
                );

                // Get X11 to update the window
                xcb_flush(graphics_info.connection);
                break;
            }
            default:
                break;
        }
        free (window_event);
    }
}

void graphics_exit() {
    xcb_disconnect(graphics_info.connection);
}


void check_for_error(xcb_void_cookie_t cookie, const char* error_message, bool is_fatal) {
    // Check for errors for the specified cookie
    xcb_generic_error_t* error = xcb_request_check (graphics_info.connection, cookie);
    if (error) {
        fprintf (stderr, error_message, error->error_code);
        free(error);
        if (is_fatal) {
            // Fatal error - exit gracefully
            graphics_exit();
            exit(EXIT_FAILURE);
        }
    }
}