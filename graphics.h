#pragma once
/*
    graphics - functions for interfacing with x11

    Copyright (C) 2019  Aidan Prangnell (AidoP)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A full copy of the license should be included with the source. If not see https://www.gnu.org/licenses/.
*/

#include <xcb/xcb.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <fontconfig/fontconfig.h>
#include <ft2build.h>
#include <xcb/render.h>
#include <xcb/xcb_renderutil.h>
#include FT_FREETYPE_H
#include "config/config.h"

// The x connection

struct {
    xcb_connection_t* connection;
    int screen_number;
    xcb_screen_t* screen;

    struct {
        uint16_t width;
        uint16_t height;
        xcb_window_t id;
        xcb_gcontext_t context_id;
    } window;

    // The pixmap to draw the panel to
    struct {
        uint16_t height;
        xcb_drawable_t id;
    } panel;

    // The program-wide font we will use
    struct {
        FT_Library library;
        FT_Face face;
        xcb_render_pictforminfo_t* format;
    } font;
} graphics_info;

typedef enum {
    cherimoya_page_home = 0,
    cherimoya_page_file_explorer = 1,
    cherimoya_page_status = 2,
    cherimoya_page_blank = 3
} cherimoya_page_type;

// A page is the underlying data of a tab
typedef struct {
    char title[256];
    bool exists;
    cherimoya_page_type page_type;
    xcb_drawable_t drawable_id;
    uint16_t width;
    uint16_t height;
} cherimoya_page;

// Cherimoya allows for up to 10 pages
cherimoya_page pages[10];
short int selected_page;

// The page to use when no other page can be found
cherimoya_page blank_page;

// Initialise and exit xcb
void graphics_init();
void graphics_exit();
void graphics_init_font();

void graphics_create_window();
void graphics_start_loop();

// Error checking in xcb is odd. This function will heavily decrease the clutter
void check_for_error(xcb_void_cookie_t cookie, const char* error_message, bool is_fatal);