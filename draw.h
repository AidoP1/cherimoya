#pragma once
/*
    draw - low level drawing with xcb

    Copyright (C) 2019  Aidan Prangnell (AidoP)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A full copy of the license should be included with the source. If not see https://www.gnu.org/licenses/.
*/

#include "graphics.h"

void set_foreground_colour(uint32_t colour);

void draw_rectangle(xcb_drawable_t drawable_id, int16_t x, int16_t y, uint16_t width, uint16_t height);

void render_character(char character, xcb_drawable_t drawable_id, int16_t x, int16_t y);

void draw_text();

// Draw the basic file explorer page
void draw_file_explorer(cherimoya_page page);

void draw_panel();