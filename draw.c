/*
    draw - low level drawing with xcb

    Copyright (C) 2019  Aidan Prangnell (AidoP)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    A full copy of the license should be included with the source. If not see https://www.gnu.org/licenses/.
*/
#include "draw.h"

void set_foreground_colour(uint32_t colour) {
    uint32_t mask_values[1] = { colour };
    xcb_change_gc(graphics_info.connection, graphics_info.window.context_id, XCB_GC_FOREGROUND, mask_values);
}

void draw_rectangle(xcb_drawable_t drawable_id, int16_t x, int16_t y, uint16_t width, uint16_t height) {
    xcb_rectangle_t rectangle[4] = { { x, y, x + width, y + height } };
    xcb_poly_fill_rectangle(graphics_info.connection, drawable_id, graphics_info.window.context_id, 1, rectangle);
}

void render_character(char character, xcb_drawable_t drawable_id, int16_t x, int16_t y) {
    // Get glyph index
    int glyph_face_index = FT_Get_Char_Index(graphics_info.font.face, character);
    if (glyph_face_index == 0)
        fprintf("Failed to render the char '%c' in the selected font face\n", character);

    // Load the raw bitmap
    FT_Select_Charmap(graphics_info.font.face, ft_encoding_unicode);
    FT_Load_Glyph(graphics_info.font.face, glyph_face_index, FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT);

    // Prepare the xcb compositing drawable
    xcb_render_picture_t glyph_picture_id = xcb_generate_id(graphics_info.connection);
    uint32_t mask_values[2] = { XCB_RENDER_POLY_MODE_IMPRECISE, XCB_RENDER_POLY_EDGE_SMOOTH };

    // Create the xcb compositing drawable
    // The freedesktop documentation is horrible so I'm guessing how to use this based on https://venam.nixers.net/blog/unix/2018/09/02/fonts-xcb.html
    xcb_render_create_picture_checked(
        graphics_info.connection,
        glyph_picture_id,
        drawable_id,
        graphics_info.font.format->id,
        XCB_RENDER_CP_POLY_MODE | XCB_RENDER_CP_POLY_EDGE,
        mask_values
    );

    // Paste the glyph on to the given drawable
    
}

void draw_file_explorer(cherimoya_page page) {
    set_foreground_colour(0xff0000);
    draw_rectangle(page.drawable_id, 0, 0, page.width, page.height);
}

void draw_panel() {
    // Get colours
    config_value background_colour;
    config_get_value_by_index(CONFIG_BACKGROUND_COLOUR, &background_colour);

    // Fill background
    set_foreground_colour(background_colour.colour);
    draw_rectangle(graphics_info.panel.id, 0, 0, graphics_info.window.width, graphics_info.panel.height);
}